window._ = require('lodash')

window.popper = require('popper.js').default;

try {
  window.$ = window.jQuery = require('jquery');

  require('bootstrap');
} catch (e) {}

// window.awesomplete = require('awesomplete');

// window.axios = require('axios');
