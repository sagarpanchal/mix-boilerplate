/**
 * https://laravel.com/docs/5.7/mix
 */
const mx = require("laravel-mix");

mx.setPublicPath('assets');

// Generate source maps when not in production
if (!mx.inProduction()) {
  mx.webpackConfig({devtool: "source-map"})
  .sourceMaps()
}
// mx.version();

// Copy fontawesome assets
// mx.copy("node_modules/@fortawesome/fontawesome-free/webfonts/", "assets/fonts");

// Compile
mx.js("resources/js/app.js", "assets/js")
.sass("resources/sass/app.scss", "assets/css");

// mx.browserSync();
